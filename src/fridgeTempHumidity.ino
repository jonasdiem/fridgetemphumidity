#include "Adafruit_DHT/Adafruit_DHT.h"
#include "MQTT.h"

#define DHTPIN 6 // temperature sensor pin
#define DHTTYPE DHT22
#define ACTectionRange 20;    //set Non-invasive AC Current Sensor tection range (20A,30A,50A,100A)
int lightSensorPin = A5; // select the input pin for the potentiometer
const int ACPin = A2;         //set arduino signal read pin

void callback(char* topic, byte* payload, unsigned int length);
float temperature;
int humidity;
float wattage;
int light = 0; // variable to store the value coming from the sensor

float temperatureArray[5];
float wattageArray[5];
int humidityArray[5];
int lightArray[5];


DHT dht(DHTPIN, DHTTYPE);
MQTT client("m13.cloudmqtt.com", 13301, callback);

//stuff for the CT
float Vref = 0;

//publishing to cloud
char publishJSONString[40];
char publishString[40];



void setup()
{

//start DHT22 temperature sensor
dht.begin();

//output variable
Particle.variable("result", publishJSONString);

//CT voltage readout
 Vref = readVref();	      //Read reference voltage

// connect to the MQTT server
    //client.connect("m13.cloudmqtt.com", "wseffhds", "tz5bGjdTFNRg");
    // publish/subscribe
    //if (client.isConnected()) {
    //    client.publish("outTopic/message","hello world");
    //    client.subscribe("inTopic/message");
    //}
}



void loop()
  {
  if (client.isConnected())
    client.loop();

for(int takeFiveReadings = 0; takeFiveReadings <5; takeFiveReadings++)
{
  temperatureArray[takeFiveReadings] = dht.getTempCelcius();
  wattageArray[takeFiveReadings]= readACCurrentValue();
  lightArray[takeFiveReadings]= analogRead(lightSensorPin);
  humidityArray[takeFiveReadings]= dht.getHumidity();
  delay(2);
}

qsort(humidityArray, 5, sizeof(humidityArray[0]), sort_desc);
qsort(wattageArray, 5, sizeof(wattageArray[0]), sort_desc);

qsort(temperatureArray, 5, sizeof(temperatureArray[0]), sort_desc);
qsort(lightArray, 5, sizeof(lightArray[0]), sort_desc);

  // Humidity measurement
  temperature = dht.getTempCelcius();

  // Humidity measurement
  humidity = humidityArray[3];

  //CT sensor
  wattage = readACCurrentValue() *230; //read AC Current Value

  //light sensor
  light = analogRead(lightSensorPin);
//  if(light > 5)


  //write JSON
  sprintf(publishJSONString, "{\"fridgeTemp\":%f,\"fridgeHum\":%i,\"fridgeWattage\":%f,\"lightSensor\":%i}", temperature, humidity, wattage, light);
  //client.publish("outTopic/message",publishJSONString);

  sprintf(publishString, "%f", temperature);
  Particle.publish("fridgeTemp", publishString, PRIVATE);
  sprintf(publishString, "%i", humidity);
  Particle.publish("fridgeHumidity", publishString, PRIVATE);
  sprintf(publishString, "%f", wattage);
  Particle.publish("fridgeWattage", publishString, PRIVATE);
  sprintf(publishString, "%i", light);
  Particle.publish("fridgeLight", publishString, PRIVATE);
  }


  float readACCurrentValue()
  {
  float ACCurrtntValue = 0;
  unsigned int peakVoltage = 0;
  unsigned int voltageVirtualValue = 0;  //Vrms

  for (int i = 0; i < 5; i++)
  {
    peakVoltage += analogRead(ACPin);   //read peak voltage
    delay(1);
  }
  peakVoltage = peakVoltage / 5;
  voltageVirtualValue = peakVoltage * 0.707;  	//change the peak voltage to the Virtual Value of voltage

  /*The circuit is amplified by 2 times, so it is divided by 2.*/
  voltageVirtualValue = (voltageVirtualValue * Vref / 1024) / 2;

  ACCurrtntValue = voltageVirtualValue * ACTectionRange;

  return ACCurrtntValue/1000;
  }



/*Read reference voltage*/
long readVref()
{
  long result;
#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega328__) || defined (__AVR_ATmega328P__)
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__) || defined(__AVR_AT90USB1286__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  ADCSRB &= ~_BV(MUX5);   // Without this the function always returns -1 on the ATmega2560 http://openenergymonitor.org/emon/node/2253#comment-11432
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#endif
#if defined(__AVR__)
  delay(2);                                        // Wait for Vref to settle
  ADCSRA |= _BV(ADSC);                             // Convert
  while (bit_is_set(ADCSRA, ADSC));
  result = ADCL;
  result |= ADCH << 8;
  result = 1126400L / result;  //1100mV*1024 ADC steps http://openenergymonitor.org/emon/node/1186
  return result;
#elif defined(__arm__)
  return (3300);                                  //Arduino Due
#else
  return (3300);                                  //Guess that other un-supported architectures will be running a 3.3V!
#endif
}


void callback(char* topic, byte* payload, unsigned int length) {
    char p[length + 1];
    memcpy(p, payload, length);
    p[length] = NULL;

    if (!strcmp(p, "RED"))
        RGB.color(255, 0, 0);
    else if (!strcmp(p, "GREEN"))
        RGB.color(0, 255, 0);
    else if (!strcmp(p, "BLUE"))
        RGB.color(0, 0, 255);
    else
        RGB.color(255, 255, 255);
    delay(1000);
}


int sort_desc(const void *cmp1, const void *cmp2)
{
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
}
